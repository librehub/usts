# usts 

Simple cli tool for creating status text to something like dwm-bar.

## Modules

| Module    | Dependencies | Description                                                                                                            |
| :---:     | :---:        | :---                                                                                                                   |
| `str`     |              | Display string.                                                                                                        |
| `time`    | `datetime`   | Display current time with strftime formating.                                                                          |
| `cpu`     | `psutil`     | Display percent of cpu usage.                                                                                          |
| `ram`     | `psutil`     | Display memory usage (percent, available, etc...)                                                                      |
| `disk`    | `os`         | Display disk usage (free, available, total)                                                                            |
| `temp`    | `psutil`     | Display temperature (cpu, gpu, etc...)                                                                                 | 
| `weather` | `requests`   | Display weather from city with openweathermap.org api. [Weather icons.](https://github.com/erikflowers/weather-icons)  |
| `xkbstate`| `subprocess` | Display keyboard layout with [xkblayout-state](https://github.com/nonpop/xkblayout-state) utility.                     | 
| `bat`     | `psutil`     | Display battery level.                                                                                                 |

## Installation

```sh
$ pip install usts
```

## ~/.config/usts/config.py example

```py
# import modules from mkmod.py file
from usts.mkmod import *
# You also can import each module at self
from usts.modules.modcpu import ModCPU

# setup list of items
items = [
    ModuleWeather("API_KEY", 
                  city="London", country="UK"),
    ModuleTemp(),
    ModuleCPU(),
    # print example -> RAM: 10.1/16.0Gb
    ModuleRAM("RAM: {used}/{total}{data_format}", data_format="Gb"),
    # print example -> /: 121.3Gb
    ModuleDisk("{path}: {free}{data_format}"),  
    ModuleTime(),
    ModuleStr("☭")
]

# Execute is action with status
#   setxroot -> setxroot -name "status"
#   stdout   -> just print status in terminal
execute = "stdout"
# Separator between modules
#   like: text1 SEPARATOR text2 SEPARATOR text3
separator = " | "
# Update periodic is sleep time between updating display
update_periodic = 1.0
```

## Usage

```sh
$ usts
```

## Contacts

| Contact                                               | Description       |
| :---:                                                 | :---              |
| [`Matrix`](https://matrix.to/#/#librehub:matrix.org)  | Matrix server.    |
| [`Discord`](https://discord.gg/naGkzRN)               | Discord server.   |

## Donates
**Monero:** `47KkgEb3agJJjSpeW1LpVi1M8fsCfREhnBCb1yib5KQgCxwb6j47XBQAamueByrLUceRinJqveZ82UCbrGqrsY9oNuZ97xN`

