import usts 
from setuptools import setup

with open("README.md", "r") as file_stream:
    readme = file_stream.read()

setup(
    long_description=readme,
    long_description_content_type="text/markdown",
    name="usts",
    version=usts.__version__,
    description="Simple cli tool for creating status text to something like dwm-bar.",
    url="https://notabug.org/loliconshik3/usts",
    author="loliconshik3",
    packages=["usts", "usts.modules"],
    entry_points={"console_scripts": ["usts = usts.__main__:main"]},
)
