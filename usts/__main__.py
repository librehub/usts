
from usts.usts import UStatus
import sys

def main():
    ustatus = UStatus()
    sys.exit(ustatus.exec())

if __name__ == "__main__":
    main()
