
from .modules.modstr import ModuleStr
from .modules.modcpu import ModuleCPU
from .modules.modram import ModuleRAM
from .modules.moddisk import ModuleDisk
from .modules.modtemp import ModuleTemp
from .modules.modtime import ModuleTime
from .modules.modbat import ModuleBattery
from .modules.modweather import ModuleWeather
from .modules.modxkbstate import ModuleXKBState

